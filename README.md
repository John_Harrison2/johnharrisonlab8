John Harrison Lab 8 ReadMe

1.	Logger.log is different from log to console since the level of its handler is to all instead of being restricted to informational messages.

2.	The line comes from the ConditionEvaluator class in Junit’s org.junit.jupiter.engine.execution module and appears because a test condition which should have been disabled was not marked as such.


3.	Assertions.assertThrows makes sure that the execution of the supplied code throws the exception expected.

4.	
    4.1.	serialVersionUID is an identifier for a class so that it may be serialized and deserialized. It is needed since in deserializing a string, the function needs to know what the members it is looking for are.
    4.2.	We override constructors to create an object with specific values for its attributes without needing to have the overhead of calling set functions for said attributes after using the default constructor.
    4.3.	We did not override other exception methods as they all will work just as needed, since the objects created by the constructors are of the same class.

5.	Whenever a Timer object is created, the static block attempts to create an input stream and read the configuration of the configuration file for the log manager. If either fails a warning is printed to the system.Out. Then an informational message is logged about starting the app.

6.	The README.md file format contains formatting syntax in order to write your file a certain way (such as ways to insert quotes, new paragraphs, etc.). In bitbucket, if your repository contains a README.md file at the root level, its contents are displayed to the source page.

7.	The test is failing due to a NullPointerException. Initializing timeNow to 0 at the beginning of the timeMe function fixes the issue.


8.	The actual issue is that the function evaluates a time of -1 and chooses to throw an exception. In order to throw, however, it must first execute the finally block. In this block the variable timeNow is accessed. Since it was null, then a NullPointerException was thrown from the function before the TimerException could be thrown. As such the exception thrown was not the one expected and the test failed.
9.  See the canvas PDF for the screenshot.
10. See the canvas PDF for the screenshot.
 

11. TimerException is a checked exception and NullPointerException is an unchecked exception.

12. Repository link: https://bitbucket.org/John_Harrison2/johnharrisonlab8/src/master/
